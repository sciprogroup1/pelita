from .utils import is_legal, cross_border_move, opposite_move
from pelita import datamodel
from pelita.graph import Graph, NoPathException, diff_pos, manhattan_dist
from pelita.player import AbstractPlayer
import numpy as np

HUNGRY_WORDS = ['So Hungry!', 'Mmmhhh', 'Delicious!']


class Bruce(AbstractPlayer):
    def set_initial(self):
        self.graph = Graph(self.current_uni.reachable([self.initial_pos]))
        self.next_food = None
        self.round_index = 0

    def goto_pos(self, pos):
        return self.graph.bfs(self.current_pos, pos)[-1]

    def food_next(self):
        fooddis = []
        for food in self.enemy_food:
            fooddis.append(manhattan_dist(self.current_pos, food))
        self.next_food = self.enemy_food[np.argmin(fooddis)]
        return self.next_food

    def home(self):
        return self.graph.bfs(self.current_pos, self.team_border)[-1]

    def get_move(self):
        self.round_index += 1

        # Get enemy position
        e_pos = (self.enemy_bots[0].current_pos,
                 self.enemy_bots[1].current_pos)
        enemy_pos = list(e_pos)
        dangerous_enemy_pos = [bot.current_pos
                               for bot in self.enemy_bots if bot.is_destroyer]
        killable_enemy_pos = [bot.current_pos
                              for bot in self.enemy_bots if bot.is_harvester]
        noisy_enemy = [bot.noisy
                       for bot in self.enemy_bots if bot.is_destroyer]
        if self.me.is_destroyer:
            # move to border from both sides at beginning of game
            if self.round_index < self.team_border[0][0]:
                if self.me.index is self.team.index:
                    next_pos = self.graph.a_star(self.current_pos,
                                                 self.team_border[0])[-1]
                else:
                    next_pos = self.graph.a_star(self.current_pos,
                                                 self.team_border[-1])[-1]
                move = diff_pos(self.current_pos, next_pos)
                # Check that it is a possible move
                move = is_legal(self, move)
            # applies for rest of the game
            else:
                if len(killable_enemy_pos) > 0:
                    next_pos = self.goto_pos(enemy_pos)
                    move = diff_pos(self.current_pos, next_pos)
                    # Check if next_pos is on the enemy side of the maze
                    if self.team.in_zone(next_pos):
                        pass
                    else:
                        move = cross_border_move(self, next_pos, move,
                                                 dangerous_enemy_pos,
                                                 noisy_enemy)
                    # Check that it is a possible move
                    move = is_legal(self, move)
                else:
                    next_pos = self.goto_pos(self.enemy_food)
                    move = diff_pos(self.current_pos, next_pos)
                    if manhattan_dist(self.current_pos,
                                      self.other_team_bots[0].current_pos) < 4:
                        next_pos = self.graph.a_star(self.current_pos,
                                                     self.initial_pos)[-1]
                        move = diff_pos(self.current_pos, next_pos)
                        # Check that it is a possible move
                        move = is_legal(self, move)
                    if not self.team.in_zone(next_pos):
                        move = cross_border_move(self, next_pos, move,
                                                 dangerous_enemy_pos,
                                                 noisy_enemy)
                        # Check that it is a possible move
                        move = is_legal(self, move)
                    if (self.team.score > 25 and (self.team.score >
                                                  (self.enemy_team.score
                                                   + 11))):
                        move = datamodel.stop
        else:
            # now we are harvester
            if (self.other_team_bots[0].is_harvester and len(killable_enemy_pos) > 0 and self.team_bots.index is self.team.index):
                # return home, if enemy in home zone and other bot harvester
                move = self.graph.a_star(self.current_pos,
                                         self.initial_pos)[-1]
                # Check that it is a possible move
                move = is_legal(self, move)
            elif (self.team.score > 25 and
                  self.team.score > (self.enemy_team.score + 11)):
                next_pos = self.goto_pos(self.team_border)
                move = diff_pos(self.current_pos, next_pos)
                # Check that it is a possible move
                move = is_legal(self, move)
            else:
                # check, if food is still present
                if (self.next_food is None or (self.next_food not in
                                               self.enemy_food)):
                    if not self.enemy_food:
                        # all food has been eaten? ok. i’ll stop
                        move = datamodel.stop
                self.next_food = self.food_next()
                next_pos = self.goto_pos(self.enemy_food)
                # check, if the next_pos has an enemy on it
                if False in noisy_enemy:
                    if np.min(len(self.graph.bfs(self.current_pos,
                                                 dangerous_enemy_pos))) < 4:
                        # better not go that way
                        self.next_food = None
                        move = opposite_move(diff_pos(self.current_pos,
                                                      next_pos))
                        if move not in self.legal_moves:
                            move = diff_pos(self.current_pos, self.home())
                    else:
                        # enemy a bit away -> can advance
                        move = diff_pos(self.current_pos, next_pos)
                else:
                    # no enemy -> can advance
                    move = diff_pos(self.current_pos, next_pos)
                    # say something if eating food
                    if next_pos in self.enemy_food:
                        self.say(self.rnd.choice(HUNGRY_WORDS))
                # Check that it is a possible move
                move = is_legal(self, move)
        try:
            return move
        except NoPathException:
            return datamodel.stop
