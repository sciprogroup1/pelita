from pelita.player import SimpleTeam
from pelita.player import SteppingPlayer

from pelita.game_master import GameMaster

from .demo_player import Bruce


def test_my_player():

    my_team = SimpleTeam("test", Bruce(), Bruce())

    test_layout = (
    """ ####################################
        #0         ##      .              1#
        #          ## ##   .               #
        #              #   .               #
        #           #      .               #
        #    .      #  #   .               #
        #              #   .               #
        #                  .               #
        #           #  #   ................#
        #2             #                  3#
        ####################################
     """
)

    teams = [
        # register my_team for bots 0, 2
        my_team,
        # other team for bots 1, 3
        SimpleTeam(SteppingPlayer("<vv<<>>^^>" * 5), SteppingPlayer("<<<<<>>>>>" * 5))
    ]

    gm = GameMaster(test_layout, teams, number_bots=4, game_time=45)

    for _ in range(100):
        # play `game_time` rounds
        gm.play()

        # check the position of my bots
        assert gm.universe.bots[0].current_pos == (27, 8)
        assert gm.universe.bots[2].current_pos == (27, 8)

        # For reference, testing the SteppingPlayer
        assert gm.universe.bots[1].current_pos == (31, 3)
        assert gm.universe.bots[3].current_pos == (29, 9)