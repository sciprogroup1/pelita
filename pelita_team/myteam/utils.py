
"""
utility functions:

    - 'is_legal' returns alternative move if intended one not legal
    - 'cross_border_move' decides whether crossing the border is smart or not
    - 'opposite_move' switches direction around
"""
import numpy as np
from pelita.graph import diff_pos
from pelita import datamodel


def is_legal(player, move):
    """ Check that it is one of all possible moves, else random move
    Parameters
    ----------
    player : object
        the player that wants to move
    move : tuple
        the move originally intended

    Returns
    ----------
    move : tuple
        the move that will be taken

    """

    if move in player.legal_moves:
        return move
    else:
        return player.rnd.choice(list(player.legal_moves.keys()))


def cross_border_move(player, next_pos, move,
                      dangerous_enemy_pos, noisy_enemy):
    """decides if crossing the border is a good idea.

    Parameters
    ----------
    player : object
        the player wanting to cross the border
    next_pos : tuple
        the next position
    move : tuple
        the move originally intended
    dangerous_enemy_pos : tuple
        position of dangerous enemies
    noisy_enemy : Bolean

    Returns
    ----------
    move : tuple
        the move that should be taken

    """

    if False in noisy_enemy:
        if np.min(len(player.graph.bfs(player.current_pos,
                                       dangerous_enemy_pos))) < 3:
            # better not go that way
            player.next_food = None
            move = opposite_move(diff_pos(player.current_pos,
                                          next_pos))
        else:
            pass
    else:
        # cross border
        move = datamodel.west
        if player.me.on_west_side:
            move = datamodel.east
        # except win is very likely, then stay in homezone!
        if (player.team.score > 25 and (player.team.score >
                                        (player.enemy_team.score + 11))):
            move = datamodel.stop
    return move


def opposite_move(move):
    """changes moves around: east to west, north to south and vice versa

    Parameters
    ----------
    move : tuple
        the original move

    Returns
    ----------
    opposite_move : tuple
        move in the other direction
    """

    lst = list(move)
    lst[0] = - lst[0]
    lst[1] = - lst[1]
    opposite_move = tuple(lst)
    return opposite_move
