# -*- coding: utf-8 -*-
"""
Created on Wed Jan 23 20:56:08 2019

@author: Andreas
"""

from pelita import datamodel
from pelita.graph import Graph, NoPathException, diff_pos, manhattan_dist
from pelita.player import AbstractPlayer, SimpleTeam
import numpy as np


class AggressivePlayer(AbstractPlayer):

    def set_initial(self):
        self.graph = Graph(self.current_uni.reachable([self.initial_pos]))

    def goto_pos(self, pos):
        return self.graph.a_star(self.current_pos, pos)[-1]

    def get_move(self):

        # Take the closest enemy
        dis = []
        for enemy in self.enemy_bots:
            dis.append(manhattan_dist(self.current_pos, enemy.current_pos))
        enemy = self.enemy_bots[np.argmin(dis)]

        try:
            next_pos = self.goto_pos(enemy.current_pos)
            # Check if the next_pos is on the wrong side of the maze
            if not self.team.in_zone(next_pos):
                # whoops, better not move
                move = datamodel.stop
            else:
                move = diff_pos(self.current_pos, next_pos)
            # Check that it is one of all possible moves, else stop
            if move in self.legal_moves:
                return move
            else:
                return datamodel.stop
        except NoPathException:
            return datamodel.stop


def team():
    return SimpleTeam('The Aggressive Players',
                      AggressivePlayer(), AggressivePlayer())
    