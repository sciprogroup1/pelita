# -*- coding: utf-8 -*-
"""
Created on Wed Jan 23 21:10:04 2019

@author: Andreas
"""

from pelita import datamodel
from pelita.graph import Graph, NoPathException, diff_pos, manhattan_dist
from pelita.player import AbstractPlayer, SimpleTeam
import numpy as np

class FastEatingPlayer(AbstractPlayer):
    """Like SmartEatingPlayer but without taking enemies into
    account and seeking the closest food (always)."""

    def set_initial(self):

        # This is called only once \t the beginning of the game
        # A graph is useful to help you find your way in the maze
        self.graph = Graph(self.current_uni.reachable([self.initial_pos]))

    def goto_pos(self, pos):

        # Next move to go to the desired position(s)
        return self.graph.bfs(self.current_pos, pos)[-1]

    def get_move(self):

        # Check the move towards the closest enemy food
        try:
            next_pos = self.goto_pos(self.enemy_food)
            move = diff_pos(self.current_pos, next_pos)
        except NoPathException:
            move = datamodel.stop

        # Check that it is one of all possible moves, else random
        if move in self.legal_moves:
            return move
        else:
            return self.rnd.choice(list(self.legal_moves.keys()))

def team():
    return SimpleTeam('The Fast Eating Players',
                      FastEatingPlayer(), FastEatingPlayer())
    