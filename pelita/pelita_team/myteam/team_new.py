# -*- coding: utf-8 -*-
"""
Created on Wed Jan 23 20:56:08 2019

@author: Andreas
"""

from pelita import datamodel
from pelita.graph import Graph, NoPathException, diff_pos, manhattan_dist
from pelita.player import AbstractPlayer
import numpy as np


class FastEatingPlayer(AbstractPlayer):
    """Like SmartEatingPlayer but without taking enemies into
    account and seeking the closest food (always)."""

    def set_initial(self):

        # This is called only once \t the beginning of the game
        # A graph is useful to help you find your way in the maze
        self.graph = Graph(self.current_uni.reachable([self.initial_pos]))
        self.next_food = None
        self.visited = set()

    def goto_pos(self, pos):

        # Next move to go to the desired position(s)
        return self.graph.bfs(self.current_pos, pos)[-1]

    def get_move(self):

        # Note that these might be noisy positions, but if one of them
        # is near us it will guaranteed not noisy
        dangerous_enemy_pos = [bot.current_pos
                               for bot in self.enemy_bots if bot.is_destroyer]
        killable_enemy_pos = [bot.current_pos
                              for bot in self.enemy_bots if bot.is_harvester]
        # Check the move towards the closest enemy food
        try:
            next_pos = self.goto_pos(self.enemy_food)
            move = diff_pos(self.current_pos, next_pos)
        except NoPathException:
            move = datamodel.stop

        # Check that it is one of all possible moves, else random
        if move in self.legal_moves:
            return move
        else:
            return self.rnd.choice(list(self.legal_moves.keys()))


class StoppingPlayer(AbstractPlayer):
    def get_move(self):
        return datamodel.stop


class AggressivePlayer(AbstractPlayer):

    def set_initial(self):
        self.graph = Graph(self.current_uni.reachable([self.initial_pos]))

    def goto_pos(self, pos):
        return self.graph.a_star(self.current_pos, pos)[-1]

    def get_move(self):

        # Take the closest enemy
        dis = []
        for enemy in self.enemy_bots:
            dis.append(manhattan_dist(self.current_pos, enemy.current_pos))
        enemy = self.enemy_bots[np.argmin(dis)]

        try:
            next_pos = self.goto_pos(enemy.current_pos)
            # Check if the next_pos is on the wrong side of the maze
            if not self.team.in_zone(next_pos):
                # whoops, better not move
                move = datamodel.stop
            else:
                move = diff_pos(self.current_pos, next_pos)
            # Check that it is one of all possible moves, else stop
            if move in self.legal_moves:
                return move
            else:
                return datamodel.stop
        except NoPathException:
            return datamodel.stop
