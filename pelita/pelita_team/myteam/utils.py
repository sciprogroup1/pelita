# This would be a good place for utility functions.


def utility_function():
    # print "Doing some hard work in this function."
    return None

def opposite_move(move):
    """changes moves around: east to west, north to south and vice versa
    
    Parameters
    ----------
    move : tuple
        the original move
        
    Returns
    ----------
    opposite_move : tuple
        move in the other direction
    """
    lst = list(move)
    lst[0] = - lst[0]
    lst[1] = - lst[1]
    opposite_move = tuple(lst)
    return 