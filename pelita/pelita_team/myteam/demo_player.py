from pelita import datamodel
from pelita.graph import Graph, NoPathException, diff_pos, manhattan_dist
from pelita.player import AbstractPlayer
import numpy as np


class SmartPlayer(AbstractPlayer):
    """Acts smart."""

    def set_initial(self):
        """This method is called only once at the begining of the game."""
        self.visited = set()
        self.graph = Graph(self.current_uni.reachable([self.initial_pos]))
        self.next_food = None

    def goto_pos(self, pos):
        return self.graph.a_star(self.current_pos, pos)[-1]

    def get_move(self):

        # Add current position to the visited locations
        self.visited.add(self.current_pos)

        # Note that these might be noisy positions, but if one of them
        # is near us it will guaranteed not noisy
        dangerous_enemy_pos = [bot.current_pos
                               for bot in self.enemy_bots if bot.is_destroyer]
        killable_enemy_pos = [bot.current_pos
                              for bot in self.enemy_bots if bot.is_harvester]

        # check if food we wanted to pick is still present
        if self.next_food is None or self.next_food not in self.enemy_food:
            if not self.enemy_food:
                # all food has been eaten? ok, game is over and i’ll stop
                return datamodel.stop
            # Then make a choice and stick to it
            self.next_food = self.rnd.choice(self.enemy_food)

        # From all possible moves pick the one which isn't visited yet
        not_visited = []
        for move, pos in self.legal_moves.items():
            if pos not in self.visited:
                not_visited.append(move)

        smart_moves = []
        for move, new_pos in list(self.legal_moves.items()):
            if move == datamodel.stop or new_pos in dangerous_enemy_pos:
                # bad idea to go into an enemy who can eat us
                continue
            elif (new_pos in killable_enemy_pos or
                  new_pos in self.enemy_food):
                # get it!
                return move
            else:
                smart_moves.append(move)

        for smart in smart_moves:
            if smart in not_visited:
                # select one of these smart ones if not visited
                return smart
            elif smart not in not_visited:
                # select either smart move if already visited
                return self.rnd.choice(smart_moves)
            else:
                # no smart moves left -> do nothing
                return datamodel.stop


#        smart_moves = []
#        for move, new_pos in list(self.legal_moves.items()):
#            if move == datamodel.stop or new_pos in dangerous_enemy_pos:
#                # bad idea to go into an enemy who can eat us
#                continue
#            elif (new_pos in killable_enemy_pos or
#                  new_pos in self.enemy_food):
#                # get it!
#                return move
#            else:
#                smart_moves.append(move)
#
#        for smart in smart_moves:
#            if smart in not_visited:
#                # select one of these smart ones if not visited
#                return smart
#            elif smart not in not_visited:
#                # select either smart move if already visited
#                return self.rnd.choice(smart_moves)
#            else:
#                # no smart moves left -> do nothing
#                return datamodel.stop
#
