from pelita import datamodel
from pelita.graph import Graph, NoPathException, diff_pos, manhattan_dist
from pelita.player import AbstractPlayer, SimpleTeam
import numpy as np

HUNGRY_WORDS = ['So Hungry!', 'Mmmhhh', 'Delicious!']


class WhateverPlayer(AbstractPlayer):
    """
    So far acts aggressively in home zone and eats nearest food in enemy zone

    All datamodel.stop might have to be replaced by something smarter.

    Smarter reactions might be needed at some other spots as well, but at least
    the players does not get timed out so far
    """
    def set_initial(self):
        self.graph = Graph(self.current_uni.reachable([self.initial_pos]))
        self.next_food = None
        self.round_index = 0
        self.score = None

    def goto_pos(self, pos):
        return self.graph.bfs(self.current_pos, pos)[-1]

    def food_next(self):
        fooddis = []
        for food in self.enemy_food:
            fooddis.append(manhattan_dist(self.current_pos, food))
        self.next_food = self.enemy_food[np.argmin(fooddis)]
        return self.next_food

    def get_move(self):
        self.round_index += 1

        dangerous_enemy_pos = [bot.current_pos
                               for bot in self.enemy_bots if bot.is_destroyer]
        noisy_enemy = [bot.noisy
                       for bot in self.enemy_bots if bot.is_destroyer]
        #  killable_enemy_pos = [bot.current_pos
        #                      for bot in self.enemy_bots if bot.is_harvester]
        if self.me.is_destroyer:
            # starts by running towards enemy zone for food in first 25 rounds
            # number of rounds can be changed or can be skipped at all if
            # better idea for start
            if self.round_index < 25:
                fooddis = []
                for food in self.enemy_food:
                    fooddis.append(manhattan_dist(self.current_pos, food))
                self.next_food = self.enemy_food[np.argmin(fooddis)]
                next_pos = self.goto_pos(self.enemy_food)
                move = diff_pos(self.current_pos, next_pos)
                return move
            else:
                # Get enemy position
                e_pos = (self.enemy_bots[0].current_pos,
                         self.enemy_bots[1].current_pos)
                enemy_pos = list(e_pos)
                try:
                    next_pos = self.goto_pos(enemy_pos)
                    # Check if the next_pos is on the enemy side of the maze
                    if not self.team.in_zone(next_pos):
                        # better solution might be needed
                        if False in noisy_enemy:
                            if np.min(len(self.graph.bfs(self.current_pos,
                                                         dangerous_enemy_pos))) < 3:
                                # better not go that way
                                self.next_food = None
                                move = opposite_move(diff_pos(self.current_pos,
                                                              next_pos))
                            else:
                                pass
                        else:
                            # cross border if no enemy waiting there
                            move = datamodel.west
                            if self.me.on_west_side:
                                move = datamodel.east
                        return move
                    # Check that it is one of all possible moves, else stop
                    if move in self.legal_moves:
                        return move
                    else:
                        return datamodel.stop
                except NoPathException:
                    return datamodel.stop
        else:
            # now we are harvester
            # check, if food is still present
            if (self.next_food is None
                    or self.next_food not in self.enemy_food):
                if not self.enemy_food:
                    # all food has been eaten? -> stop
                    return datamodel.stop
            self.next_food = self.food_next()
            try:
                next_pos = self.goto_pos(self.enemy_food)
                # check, if the next_pos has an enemy on it
                if False in noisy_enemy:
                    if np.min(len(self.graph.bfs(self.current_pos,
                                                 dangerous_enemy_pos))) < 3:
                        # better not go that way
                        self.next_food = None
                        move = opposite_move(diff_pos(self.current_pos,
                                                      next_pos))
                    else:
                        # enemy a bit away
                        pass
                        move = diff_pos(self.current_pos, next_pos)
                else:
                    # no enemy -> can advance
                    move = diff_pos(self.current_pos, next_pos)
                    # say something if eating food
                    if next_pos in self.enemy_food:
                        self.say(self.rnd.choice(HUNGRY_WORDS))
                if move in self.legal_moves:
                    return move
                else:
                    return datamodel.stop
            except NoPathException:
                return datamodel.stop


# added function that reverses a move 180�
def opposite_move(tpl):
    """changes moves around: east to west, north to south and vice versa

    Parameters
    ----------
    move : tuple
        the original move

    Returns
    ----------
    opposite_move : tuple
        move in the other direction
    """

    lst = list(move)
    lst[0] = - lst[0]
    lst[1] = - lst[1]
    opposite_move = tuple(lst)
    return opposite_move


def team():
    return SimpleTeam("Whatever Players", WhateverPlayer(), WhateverPlayer())
