# -*- coding: utf-8 -*-
"""
Created on Wed Jan 23 22:33:41 2019

@author: Andreas

returning datamodel.stop might not always be the most elegant idea, but
had no better one for the moment.

Also running away fro enemy does not always work as intended, but no
timeouts resulted in most of the last games

Some inline commands need to be removed before handing in!!!

"""

from pelita import datamodel
from pelita.graph import Graph, NoPathException, diff_pos, manhattan_dist
from pelita.player import AbstractPlayer, SimpleTeam
import numpy as np

HUNGRY_WORDS = ['So Hungry!', 'Mmmhhh', 'Delicious!']


class WhateverPlayer(AbstractPlayer):
    def set_initial(self):
        self.graph = Graph(self.current_uni.reachable([self.initial_pos]))
        self.next_food = None
        self.round_index = 0

    def goto_pos(self, pos):
        # finds positiondesired to move to
        return self.graph.bfs(self.current_pos, pos)[-1]

    def food_next(self):
        # closest food
        fooddis = []
        for food in self.enemy_food:
            fooddis.append(manhattan_dist(self.current_pos, food))
        self.next_food = self.enemy_food[np.argmin(fooddis)]
        return self.next_food

    def home(self):
        # used to make player return home
        return self.graph.bfs(self.current_pos, self.team_border)[-1]

    def middle_pos(self):

        # used to find centre of home zone
        a = int(max(self.initial_pos[0]/2, self.initial_pos[0]/2))
        b = self.team_border[4][1]
        return (a, b)
    
    def get_move(self):
        self.round_index += 1

        # Get enemy position
        e_pos = (self.enemy_bots[0].current_pos,
                 self.enemy_bots[1].current_pos)
        enemy_pos = list(e_pos)
        dangerous_enemy_pos = [bot.current_pos
                               for bot in self.enemy_bots if bot.is_destroyer]
        killable_enemy_pos = [bot.current_pos
                              for bot in self.enemy_bots if bot.is_harvester]
        noisy_enemy = [bot.noisy
                       for bot in self.enemy_bots if bot.is_destroyer]
        if self.me.is_destroyer:
            if self.round_index < 15:
                if self.me.index is self.team.index:
                    next_pos = self.graph.a_star(self.current_pos,
                                                 self.team_border[0])[-1]
                else:
                    next_pos = self.graph.a_star(self.current_pos,
                                                 self.team_border[-1])[-1]
                move = diff_pos(self.current_pos, next_pos)
                return move
            else:
                if len(killable_enemy_pos) > 0:
                    try:
                        next_pos = self.goto_pos(enemy_pos)
                        move = diff_pos(self.current_pos, next_pos)
                        # Check if next_pos is on the enemy side of the maze
                        if not self.team.in_zone(next_pos):
                            # now as own function
                            move = cross_border_move(self, next_pos, move,
                                                     dangerous_enemy_pos,
                                                     noisy_enemy)
                        # Check that it is a possible move -> also function now
                        move = is_legal(self, move)
                    except NoPathException:
                        move = datamodel.stop
                else:
                    next_pos = self.goto_pos(self.enemy_food)
                    move = diff_pos(self.current_pos, next_pos)
                    # checks if bots are too close together
                    if manhattan_dist(self.current_pos,
                                      self.other_team_bots[0].current_pos) < 4:
                        next_pos = self.graph.a_star(self.current_pos,
                                                     self.middle_pos())[-1]
                        move = diff_pos(self.current_pos, next_pos)
                        # Check that it is a possible move
                        move = is_legal(self, move)
                    # again check for border crossing in function
                    if not self.team.in_zone(next_pos):
                        move = cross_border_move(self, next_pos, move,
                                                 dangerous_enemy_pos,
                                                 noisy_enemy)
                    if (self.team.score > 25 and
                            self.team.score > (self.enemy_team.score + 11)):
                        move = datamodel.stop
                return move
        else:
            # now we are harvester
            if (self.other_team_bots[0].is_harvester and
                    len(killable_enemy_pos) > 0 and
                    self.team_bots.index is self.team.index):
                # return home, if enemy in home zone and other bot harvester
                # -> not working properly at the moment
                move = self.graph.a_star(self.current_pos,
                                         self.initial_pos)[-1]
                return move
            elif (self.team.score > 25 and
                  self.team.score > (self.enemy_team.score + 11)):

                try:
                    next_pos = self.goto_pos(self.team_border)
                    move = diff_pos(self.current_pos, next_pos)
                    # Again: Check for possible move in function
                    move = is_legal(self, move)
                    return move
                except NoPathException:
                    return datamodel.stop
            else:
                # check, if food is still present
                if (self.next_food is None or
                        self.next_food not in self.enemy_food):
                    if not self.enemy_food:
                        # all food has been eaten? stop
                        return datamodel.stop
                self.next_food = self.food_next()
                try:
                    next_pos = self.goto_pos(self.enemy_food)
                    # check, if the next_pos has an enemy on it
                    if False in noisy_enemy:
                        if np.min(len(self.graph.bfs(
                                self.current_pos, dangerous_enemy_pos))) < 3:
                            # better not go that way
                            self.next_food = None
                            move = opposite_move(diff_pos(self.current_pos,
                                                          next_pos))
                            if move not in self.legal_moves:
                                # return home if not a legal move
                                move = diff_pos(self.current_pos, self.home())
                        else:
                            # enemy a bit away
                            move = diff_pos(self.current_pos, next_pos)
                    else:
                        # no enemy -> can advance
                        move = diff_pos(self.current_pos, next_pos)
                        # say something if eating food
                        if next_pos in self.enemy_food:
                            self.say(self.rnd.choice(HUNGRY_WORDS))
                    # Again legality check
                    move = is_legal(self, move)
                    return move
                except NoPathException:
                    return datamodel.stop


# probably these functions should be stored in utils.py!?

def is_legal(player, move):
    """ Check that it is one of all possible moves, else random move
    Parameters
    ----------
    player : object
        the player that wants to move
    move : tuple
        the move originally intended

    Returns
    ----------
    move : tuple
        the move that will be taken

    """

    if move in player.legal_moves:
        return move
    else:
        return player.rnd.choice(list(player.legal_moves.keys()))


def cross_border_move(player, next_pos, move,
                      dangerous_enemy_pos, noisy_enemy):
    """decides if crossing the border is a good idea.

    Parameters
    ----------
    player : object
        the player wanting to cross the border
    next_pos : tuple
        the next position
    move : tuple
        the move originally intended
    dangerous_enemy_pos : tuple
        position of dangerous enemies
    noisy_enemy : Bolean

    Returns
    ----------
    move : tuple
        the move that should be taken

    """

    if False in noisy_enemy:
        if np.min(len(player.graph.bfs(player.current_pos,
                                       dangerous_enemy_pos))) < 3:
            # better not go that way
            player.next_food = None
            move = opposite_move(diff_pos(player.current_pos,
                                          next_pos))
        else:
            pass
    else:
        # cross border
        move = datamodel.west
        if player.me.on_west_side:
            move = datamodel.east
        # except win is very likely, then stay in homezone!
        if (player.team.score > 25 and (player.team.score >
                                        (player.enemy_team.score + 11))):
            move = datamodel.stop
    return move


def opposite_move(move):
    """changes moves around: east to west, north to south and vice versa
    might have to be improved as avoiding enemy sometimes does not work...

    Parameters
    ----------
    move : tuple
        the original move

    Returns
    ----------
    opposite_move : tuple
        move in the other direction
    """

    lst = list(move)
    lst[0] = - lst[0]
    lst[1] = - lst[1]
    opposite_move = tuple(lst)
    return opposite_move


def team():
    return SimpleTeam("Whatever Players", WhateverPlayer(), WhateverPlayer())
