# -*- coding: utf-8 -*-
"""
Created on Wed Jan 23 22:33:41 2019

@author: Andreas
"""

from pelita import datamodel
from pelita.graph import Graph, NoPathException, diff_pos, manhattan_dist
from pelita.player import AbstractPlayer, SimpleTeam
import numpy as np

HUNGRY_WORDS = ['So Hungry!', 'Mmmhhh', 'Delicious!']


class WhateverPlayer(AbstractPlayer):
    def set_initial(self):
        self.graph = Graph(self.current_uni.reachable([self.initial_pos]))
        self.next_food = None
        self.round_index = 0

    def goto_pos(self, pos):
        return self.graph.bfs(self.current_pos, pos)[-1]

    def food_next(self):
        fooddis = []
        for food in self.enemy_food:
            fooddis.append(manhattan_dist(self.current_pos, food))
        self.next_food = self.enemy_food[np.argmin(fooddis)]
        return self.next_food

    def get_move(self):
        self.round_index += 1

        # Get enemy position
        e_pos = (self.enemy_bots[0].current_pos,
                 self.enemy_bots[1].current_pos)
        enemy_pos = list(e_pos)
        dangerous_enemy_pos = [bot.current_pos
                               for bot in self.enemy_bots if bot.is_destroyer]
        killable_enemy_pos = [bot.current_pos
                              for bot in self.enemy_bots if bot.is_harvester]
        noisy_enemy = [bot.noisy
                       for bot in self.enemy_bots if bot.is_destroyer]
        if self.me.is_destroyer:
            if self.round_index < 15:
                if self.me.index is self.team.index:
                    next_pos = self.graph.a_star(self.current_pos,
                                                 self.team_border[0])[-1]
                else:
                    next_pos = self.graph.a_star(self.current_pos,
                                                 self.team_border[-1])[-1]
                move = diff_pos(self.current_pos, next_pos)
                return move
            elif self.round_index < 30:
                next_pos = self.goto_pos(self.enemy_food)
                move = diff_pos(self.current_pos, next_pos)
                return move
            else:
                if len(killable_enemy_pos) > 0:
                    try:
                        next_pos = self.goto_pos(enemy_pos)
                        move = diff_pos(self.current_pos, next_pos)
                        # Check if next_pos is on the enemy side of the maze
                        if not self.team.in_zone(next_pos):
                            if False in noisy_enemy:
                                if np.min(len(self.graph.bfs(self.current_pos,
                                                             dangerous_enemy_pos))) < 4:
                                    # better not go that way
                                    self.next_food = None
                                    move = opposite_move(diff_pos(self.current_pos,
                                                                  next_pos))
                                else:
                                    pass
                            else:
                                move = datamodel.west
                                if self.me.on_west_side:
                                    move = datamodel.east
                                if (self.team.score > 25 and self.team.score > (self.enemy_team.score + 11)):
                                    move = datamodel.stop
                            return move
                        # Check that it is one of all possible moves, else stop
                        if move in self.legal_moves:
                            return move
                        else:
                            return datamodel.stop
                    except NoPathException:
                        return datamodel.stop
                else:
                    next_pos = self.goto_pos(self.enemy_food)
                    move = diff_pos(self.current_pos, next_pos)
                    if (self.team.score > 25 and self.team.score > (self.enemy_team.score + 11)):
                        move = datamodel.stop
                return move
        else:
            # now we are harvester
            if (self.other_team_bots[0].is_harvester and len(killable_enemy_pos) > 0 and self.team_bots.index is self.team.index):
                # return home, if enemy in home zone and other bot harvester
                move = self.graph.a_star(self.current_pos,
                                         self.initial_pos)[-1]
                return move
            elif (self.team.score > 25 and
                  self.team.score > (self.enemy_team.score + 11)):
                
                try:
                    next_pos = self.goto_pos(self.team_border)
                    move = diff_pos(self.current_pos, next_pos)
                    if move in self.legal_moves:
                        return move
                    else:
                        return self.rnd.choice(list(self.legal_moves.keys()))
                except NoPathException:
                    return datamodel.stop
            else:
                # check, if food is still present
                if (self.next_food is None or self.next_food not in self.enemy_food):
                    if not self.enemy_food:
                        # all food has been eaten? ok. i’ll stop
                        return datamodel.stop
                self.next_food = self.food_next()
                try:
                    next_pos = self.goto_pos(self.enemy_food)
                    # check, if the next_pos has an enemy on it
                    if False in noisy_enemy:
                        if np.min(len(self.graph.bfs(self.current_pos,
                                                     dangerous_enemy_pos))) < 3:
                            # better not go that way
                            self.next_food = None
                            move = opposite_move(diff_pos(self.current_pos,
                                                          next_pos))
                        else:
                            # enemy a bit away
                            pass
                            move = diff_pos(self.current_pos, next_pos)
                    else:
                        # no enemy -> can advance
                        move = diff_pos(self.current_pos, next_pos)
                        # say something if eating food
                        if next_pos in self.enemy_food:
                            self.say(self.rnd.choice(HUNGRY_WORDS))
                    if move in self.legal_moves:
                        return move
                    else:
                        return datamodel.stop
                except NoPathException:
                    return datamodel.stop



def opposite_move(move):
    """changes moves around: east to west, north to south and vice versa

    Parameters
    ----------
    move : tuple
        the original move

    Returns
    ----------
    opposite_move : tuple
        move in the other direction
    """

    lst = list(move)
    lst[0] = - lst[0]
    lst[1] = - lst[1]
    opposite_move = tuple(lst)
    return opposite_move


def team():
    return SimpleTeam("Whatever Players", WhateverPlayer(), WhateverPlayer())
